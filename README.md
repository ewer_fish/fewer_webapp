# mFisheries Web App


## Introduction
The web app is the client-side JavaScript application that consume the services provided by the mFisheries web services. 

The web app is built with code JS ES2016 and compiled to ES5 using babel.js

## Use
The code is used within ionic to wrap and build mobile specific version
* [Ionic Framework](https://ionicframework.com/)
* [Ionic Instructions] (https://ionicframework.com/docs/intro/installation/)

## Future Work
This code will eventually become including into the main mFisheries system as a [git submodule](https://git-scm.com/docs/git-submodule)