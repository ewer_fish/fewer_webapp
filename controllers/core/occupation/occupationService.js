"use strict";

/**
 *
 */
class Occupations{
	/**
	 *
	 * @param $http
	 */
	constructor($http){
		this.$http = $http;
		this.base_url = "/api/occupations";
	}
	
	/**
	 *
	 * @param countryid
	 */
	get (countryid) {
		const options = {};
		if (countryid)options.countryid = countryid;
		return this.$http.get(this.base_url, {params:options});
	}
	
	/**
	 *
	 * @param data
	 */
	add(data){
		return this.$http.post(this.base_url, data);
	}
	
	/**
	 *
	 * @param source
	 */
	update(source) {
		return this.$http.put(this.base_url + "/" + source.id, source);
	}
	
	/**
	 *
	 * @param source
	 */
	delete(source){
		return this.$http.delete(this.base_url+ "/" + source.id);
	}
}

Occupations.$inject = [
	"$http"
];
angular.module('mfisheries.Occupation').service("Occupations", Occupations);
