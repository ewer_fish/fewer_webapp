"use strict";
// newUserInfoCtrl.sj
// group controller name  : newUserInfoController
// namesapce : mfisheries

/*
 *
 *	controller name newUserInfo
 *	This controller uses the LocalStorage service
 *	$scope object and the service which was created to get the new user info
 */
angular.module('mfisheries.User')
.controller('newUserInfo', function($scope, LocalStorage, getNewUserInfo){
	// initialize the newinfo scope object

	$scope.newInfo = {};
	getNewUserInfo.get(LocalStorage.getObject('user').userid)
	.then(function(res){
		
		$scope.newInfo = res.data.data;
	},
	function(res){
		// if there is an error display 
		// relevant message
		console.log('error');
	});

});