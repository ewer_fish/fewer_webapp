'use strict';


angular.module('mfisheries.User')

.service("getImageDates", function($http){
	
	var deffered = {};

	deffered.get = function(userid){
		return $http.get('api/get/image/dates/'+userid);
	};

	return deffered;
})



.service("getLastImage", function($http){

	var deffered = {};

	deffered.get = function(userid, day, month, year){
		console.log('api/get/last/images/'+userid+'/'+day+'/'+month+'/'+year);
		return $http.get('api/get/last/images/'+userid+'/'+day+'/'+month+'/'+year);
	};

	return deffered;
})

.service("getUserImages", function($http){

	var deffered = {};

	deffered.get = function(userid){
		return $http.get('api/get/user/images/'+userid);
	};

	return deffered;
})

.service("UserProfile", ['$http','Session', function($http, Session){
	var userProfile = {};

	userProfile.get = function(userid){
		return $http.get('/api/get/user/full/profile/'+userid);
	};

	userProfile.post = function(data) {
		return $http
				.post('/api/update/user/profile', data);
	};

	//converter (Translate Data Representation to Form format)
	userProfile.convert = function(usr) {
		var resUsr = { user: {} };
		if (!resUsr.user.initial) resUsr.user.initial = {};

		if (usr.countryid) {
			resUsr.user.initial.countryid = usr.countryid;
			resUsr.selectedCountry = usr.countryid;
		}

		if (usr.mobileNum)
			resUsr.user.initial.mobileNum = usr.mobileNum;

		if (!resUsr.user.basic)resUsr.user.basic = {};
		if (usr.age)
			resUsr.user.basic.age = usr.age;
		// TODO Occupations
		if (usr.occupations){
			resUsr.user.basic.occupations = {};
			// http://underscorejs.org/#each
			_.each(usr.occupations, function(o){
				resUsr.user.basic.occupations[o.occupationid] = o.occupationid;
			});
		}

		if (!resUsr.user.standard)resUsr.user.standard = {};
		if (usr.hstreet)
			resUsr.user.standard.hstreet = usr.hstreet;
		if (usr.villageid) {
			resUsr.user.standard.village = usr.villageid;
			resUsr.selectedVillage = usr.villageid;
		}

		if (!resUsr.user.full)resUsr.user.full = {};
		if (usr.nat_id)
			resUsr.user.full.nat_id = usr.nat_id;
		if (usr.nat_dp)
			resUsr.user.full.nat_dp = usr.nat_dp;
		if (usr.passport)
			resUsr.user.full.passport = usr.passport;

		if (!resUsr.user.full.emg)resUsr.user.full.emg = {};
		if (usr.emg) {
			var emg = usr.emg[0];
			resUsr.user.full.emg.fname = emg.fname;
			resUsr.user.full.emg.lname = emg.lname;
			resUsr.user.full.emg.homenum = emg.homenum;
			resUsr.user.full.emg.cellnum = emg.cellnum;
			resUsr.user.full.emg.street = emg.street;
			resUsr.user.full.emg.village = emg.villageid;
			resUsr.user.full.emg.id = emg.emergencyid;
			resUsr.user.full.emg.email = emg.email;
		}

		if (!resUsr.user.standard.vessel)resUsr.user.standard.vessel = {};
		if (usr.vessel) {
			var vessel = usr.vessel[0];
			resUsr.user.standard.vessel.insidecolor = vessel.insidecolor;
			resUsr.user.standard.vessel.description = vessel.description;
			resUsr.user.standard.vessel.length = vessel.length;
			resUsr.user.standard.vessel.regnumber = vessel.regnumber;
			resUsr.user.standard.vessel.id = vessel.vesselid;
			resUsr.user.standard.vessel.hullcolor = vessel.hullcolor;
			resUsr.user.standard.vessel.hulltype = vessel.hulltype;
		}
		return resUsr;
	};

	return userProfile;
}]);


