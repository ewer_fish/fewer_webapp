
class FEWERCtrl extends BaseController{
	
	constructor(Country, Locator, AuthService){
		super(Country, Locator, AuthService);
	}
}
FEWERCtrl.$inject = [
	"Country",
	"Locator",
	"AuthService"
];
angular
	.module('mfisheries.FEWERModule', [])
	.controller("FEWERCtrl", FEWERCtrl);