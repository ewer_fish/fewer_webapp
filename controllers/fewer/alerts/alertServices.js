class Alerts{
	constructor($http){
		this.$http = $http;
		this.base_url = "/api/alerts";
	}
	get(countryid, allAlert){
		const options = { type:"public" };
		if (countryid && countryid !== 0)options.countryid = countryid;
		if (allAlert)delete options.type;
		return this.$http.get(this.base_url, { params: options });
	}
	
	getByGroup(groupid){
		const url = this.base_url + "?group=" + groupid;
		console.log("Attempting to retrieve alerts from: " + url);
		return this.$http.get(url);
	}
	
	add(data){
		return this.$http.post(this.base_url, data);
	}
	update(source) {
		return this.$http.put(this.base_url + "/" + source.id, source);
	}
	delete(source){
		return this.$http.delete(this.base_url+ "/" + source.id);
	}
}

Alerts.$inject = [
	"$http"
];
angular.module('mfisheries.CAP').service("Alerts", Alerts);
