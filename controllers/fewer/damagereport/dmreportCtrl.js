"use strict";

// Class for controller based on https://coryrylan.com/blog/es2015-class-in-angularjs-controllers-and-services
/**
 * Class for DamageReport controller.
 * The same class is used for both viewing and administrative functionality
 */
class DMReportCtrl {

	/**
	 *
	 * @param DMReport
	 * @param DMCategory
	 * @param AuthService
	 * @param Country
	 * @param Locator
	 */
	constructor(DMReport, DMCategory, AuthService, Country, Locator) {
		this.reports = [];
		this.report = {};
		this.DMReport = DMReport;
		this.is_loading = true;

		this.audience = [
			{'id': '', 'name': "Select Audience"},
			{'id': 1, 'name': "Public"},
			{'id': 0, 'name': "Private"}
		];

		this.AuthService = AuthService;

		// country related attributes
		this.Country = Country;
		this.currLocation = ["", ""];
		this.countries = [];
		this.userCountry = 0;

		// location related attributes
		this.locator = Locator;

		// Categories related attributes
		this.categories = [];
		this.category = {};
		this.DMCategory = DMCategory;
		this.is_cat_loading = true;

		this.init();
	}

	/**
	 *
	 */
	init() {
		$(".mFish_menu").removeClass("active");
		$("#menu-fewer").addClass("active");
		$("#menu-dmreport").addClass("active");

		// Check if user is signed in
		this.AuthService.attachCurrUser(this).then(currUser => {
			if (currUser)console.log("User is logged in");
			else console.log("User is not logged in");
		});

		// this.getLocation();
	}

	/**
	 *
	 */
	getLocation() {
		console.log("Attempting to load the current location");
		const self = this;
		this.locator.retrieveCurrentLocation().then(location => {
			// console.log("Received:" + JSON.stringify(location));
			self.currLocation[0] = location.coords.latitude;
			self.currLocation[1] = location.coords.longitude;
			self.updateReportLocation();
		}, err => {
			console.log("unable to retrieve location");
			console.log(err);
		});
	}

	/**
	 *
	 */
	updateReportLocation() {
		this.report.latitude = this.currLocation[0];
		this.report.longitude = this.currLocation[1];
	}

	/**
	 *
	 */
	reset() {
		this.report = {
			"id": 0,
			"name": "",
			"description": "",
			"userid": this.currentUser.userid,
			"countryid": this.currentUser.countryid,
			"latitude": this.currLocation[0],
			"longitude": this.currLocation[1],
			"createdby": this.currentUser.userid,
			"filetype": "text",
			"filepath": "",
			"aDate": new Date(),
			"cost": 0,
			"category": "Environment",
			"isPublic": "0"
		};
	}

	/**
	 *
	 */
	resetCategory() {
		this.category = {
			"id": 0,
			"name": "",
			"countryid": this.currentUser.countryid
		};
	}

	/**
	 *
	 */
	startAdmin() {
		this.reset();
		this.resetCategory();
		this.retrieveReports();
		this.retrieveCategories();
		this.retrieveCountries();
	}

	/**
	 *
	 */
	startView() {
		console.log("starting Damage Report Administrative Tasks");
		// display a loading message while we request country data
		this.displayLoading("Loading Countries").then(loading =>{
			// After Displaying notification
			this.retrieveCountries(true).then(countries =>{
				loading.close();
				this.locator.getLastLocationID().then(countryid => {
					console.log("Location found from cache as: " + countryid);
					if (countryid && countryid !== "undefined")this.handleCountryChange(countryid);
				});
			}, err => loading.close());
		});

		// this.retrieveCountries(true);
		// this.retrieveReports();
	}
	
	retrieveReports(countryid) {
		let country  = (countryid) ? countryid : this.userCountry;
		this.is_loading = true;
		console.log("Attempting to retrieve damage report for %s" , countryid);
		this.DMReport.get(country).then(res => {
			this.is_loading = false;
			this.reports = res.data.map(report => {
				report.aDate = moment(report.aDate).toDate();
				return report;
			});
			console.log("Received %s reports from %s", this.reports.length, countryid);
		});
	}
	
	retrieveCategories() {
		this.is_cat_loading = true;
		this.DMCategory.get(this.userCountry).then(res => {
			this.is_cat_loading = false;
			this.categories = res.data;
			console.log("Received %s categories", res.data.length);
		});
	}
	
	retrieveCountries(display) {
		return new Promise((resolve, reject) => {
			const self = this;
			this.Country.get(this.userCountry).then(res => {
				self.countries = res.data;
				if (display) this.countries.unshift({ id: 0, name: "Select Country"});
				resolve(res.data);
			}, reject);
		});
	}
	
	displayAdd() {
		this.reset();
		const modelModal = $("#modelModal");
		modelModal.modal('show');
		modelModal.updatePolyfill();
	}
	
	addCategory() {
		this.resetCategory();
		const modelModal = $("#categoryModal");
		modelModal.modal('show');
		modelModal.updatePolyfill();
	}
	
	edit(report) {
		console.log("edit - " + report.id);
		
		if (typeof(report.aDate) === 'string') {
			report.aDate = moment(report.aDate).toDate();
			console.log(typeof report.aDate);
		}
		this.report = report;
		const modelModal = $("#modelModal");
		modelModal.modal('show');
		modelModal.updatePolyfill();
	}
	
	view(report) {
		console.log("view " + report.id);
		this.report = report;
		const modelModal = $("#modelModal");
		modelModal.modal('show');
		modelModal.updatePolyfill();
	}
	
	deleteRec(report, idx) {
		const self = this;
		console.log("delete " + report.id + " at index " + idx);
		swal({
			title: "Delete Confirmation",
			text: "Are you sure you want to delete this record. You will not be able to undo this operation",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Delete",
			closeOnConfirm: false
		}).then(result => {
			if (result.value) {
				self.DMReport.delete(report).then(res => {
					swal("Delete Record", "Record was successfully deleted", "success");
					self.reports.splice(idx, 1);
				}, err => {
					swal("Delete Record", "Unable to delete record", "error");
					console.log(err);
				});
			}else console.log("Opted not to delete damage report");
		});
	}
	
	save(report) {
		console.log(report);
		
		report.aDate = (new Date(report.aDate)).getTime();
		if (report.id && report.id > 0) {
			this.DMReport.update(report).then(() => {
				swal("Report", "Report was successfully created", "success");
				$("#modelModal").modal('hide');
			}, (err) => {
				swal("Update Report", "Unable to update report. If error persist contact administrator for support.", "error");
				console.log(err);
			});
		} else {
			this.DMReport.add(report).then((res) => {
				if (res.status === 201) {
					swal("Report", "Report was successfully created", "success");
					$("#modelModal").modal('hide');
					this.retrieveReports();
				} else swal("Report", "Unable to save report", "error");
			}, (err) => {
				swal("Report", "Unable to save report. If error persist contact administrator for support.", "error");
				console.log(err);
			});
		}
	}
	
	saveCategory(category) {
		if (category.id > 0) {
			this.DMCategory.update(category).then(() => {
				swal("Update category", "Category was successfully created", "success");
				$("#categoryModal").modal('hide');
			}, (err) => {
				swal("Update category", "Unable to update report. If error persist contact administrator for support.", "error");
				console.log(err);
			});
		} else {
			this.DMCategory.add(category).then((res) => {
				if (res.status === 201) {
					swal("Saving Category", "Category was successfully created", "success");
					$("#categoryModal").modal('hide');
					this.retrieveCategories();
				} else swal("Saving Category", "Unable to save category", "error");
			}, (err) => {
				swal("Saving Category", "Unable to save category. If error persist contact administrator for support.", "error");
				console.log(err);
			});
		}
	}
	
	editCat(category){
		console.log("edit - " + category.id);
		this.category = category;
		const modelModal = $("#categoryModal");
		modelModal.modal('show');
		modelModal.updatePolyfill();
	}
	
	deleteCat(category, idx){
		const self = this;
		console.log("delete " + category.id + " at index " + idx);
		swal({
			title: "Delete Confirmation",
			text: "Are you sure you want to delete this record. You will not be able to undo this operation",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Delete",
			closeOnConfirm: false
		}).then(result => {
			if (result.value) {
				self.DMCategory.delete(category).then(() => {
					swal("Delete Category", "Category was successfully deleted", "success");
					self.categories.splice(idx, 1);
				}, err => {
					swal("Delete Category", "Unable to delete category", "error");
					console.log(err);
				});
			}
		});
	}
	
	handleCountryChange(countryid){
		console.log("Country was changed to:" + countryid);
		if (!isNaN(countryid))countryid = parseInt(countryid);
		this.userCountry = countryid;

		this.locator.saveLastLocationID(countryid).then(res => console.log(res));
		// Attempting to add country name to be displayed based on data
		let country = this.countries.filter(country => country.id === countryid)[0];
		if (country)this.countryname = country.name;
		// retrieve posts
		this.retrieveReports(countryid);
	}

	/**
	 * Displays the a control-less loading message dialog.
	 * Clients that use the function should use the extent and call close
	 * method on the response to dismiss message
	 * @param message
	 * @returns {Promise}
	 */
	displayLoading(message){
		swal({
			type: "info",
			title: "Weather",
			text: message,
			allowOutsideClick: false,
			allowEscapeKey: false,
			showConfirmButton: false,
			onOpen: function () {
				swal.showLoading();
			}
		});

		return Promise.resolve(swal);
	}
}

DMReportCtrl.$inject = [
	"DMReport",
	"DMCategory",
	"AuthService",
	"Country",
	"Locator"
];

angular.module('mfisheries.DMReport', [])
	.controller("DMReportCtrl", DMReportCtrl);