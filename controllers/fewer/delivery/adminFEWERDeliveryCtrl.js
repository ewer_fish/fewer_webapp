
angular.module('mfisheries.FEWERModule')
	.controller('adminFEWERDeliveryCtrl',['$scope', '$firebaseArray', function($scope, $firebaseArray){

		console.log("FEWER Admin Alerts Delivery Confirmation Controller Loaded");
		
		$(".mFish_menu").removeClass("active");
		$("#menu-fewer").addClass("active");

		firebase.auth().signInAnonymously().catch(function(error) {
		  // Handle Errors here.
		  var errorCode = error.code;
		  var errorMessage = error.message;
		  console.log(errorCode + ": " + errorMessage);
		});

		firebase.auth().onAuthStateChanged(function(user) {
		  if (user) {
		    // User is signed in.
				var ref = firebase.database().ref().child('notif-status');
		  	$scope.statuses = $firebaseArray(ref);
		  } else {
		    // User is signed out.
		  }
		});

	}]);
