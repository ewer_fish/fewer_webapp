angular.module('mfisheries.FEWERModule')
	.controller('FEWERSrcMgmtCtrl', ['$scope', 'LocalStorage', 'CAPSource', 'AuthService', '$routeParams', '$location', '$sce',
		function ($scope, LocalStorage, CAPSource, AuthService, $routeParams, $location, $sce) {
			"use strict";
			// Ensure User is logged in correctly
			AuthService.attachCurrUser($scope);

			// Retrieve Parameters to Setup Page Operations
			$scope.operation = $routeParams.operation;
			$scope.src_id = $routeParams.src_id;
			
			loadSource( $routeParams.src_id);
			
			
			
			function loadSources() {
				CAPSource.get($scope.userCountry).then(function (res) {
					if (res.data && res.data instanceof Array) {
						$scope.sources = res.data;
					}
				});
			}
			
			function loadSource(src_id) {
				CAPSource.get_one(src_id).then((res)=>{
					if (res.status === 200)processSource(res.data);
				}, (err)=> {
					console.log(err);
					redirectWhenError();
				});
			}
			
			function processSource(source){
				// Check to ensure that user has appropriate permission to view source
				console.log($scope.userCountry);
				$scope.source = source;
				if ($scope.userCountry !== 0){
					if (source.countryid !== $scope.userCountry){
						console.log("Unable to access source");
						redirectWhenError();
						return;
					}
				}
				// If we get to this point we have access to the source
				if ($scope.operation === "manage" ){
					if(source.category === "CAP" && source.scope === "mFisheries"){
						const url = getManageAdminURL(source);
						$scope.source.used_url = $sce.trustAsResourceUrl(url);
						console.log($scope.source.used_url);
					}else{
						swal("Manage Source",
							"No Management Operations available for selected source",
							"error");
						redirectWhenError();
						return;
					}
				}else if ($scope.operation === "view"){
						$scope.source.used_url = $sce.trustAsResourceUrl($scope.source.url);
				}else{
					swal(
						"Source",
						"Invalid Operation",
						"error"
					);
					redirectWhenError();
				}
			}
			
			function redirectWhenError(){
				$location.path("/fewer/sources");
			}
			
			function getManageAdminURL(source){
				let url = source.url;
				let urlComponents = url.split("/");
				urlComponents = urlComponents.slice(0, -1);
				let source_url = urlComponents.join("/");
				source_url += "/admin";
				return source_url;
				
			}
			
		}]);