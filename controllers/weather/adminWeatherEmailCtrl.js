"use strict";

angular.module('mfisheries.Weather')
	.controller('adminWeatherEmailCtrl', [
		'$scope', 'LocalStorage', '$compile', 'Country','CountryModule','Emailing','AuthService',
		function ($scope, LocalStorage, $compile, Country,CountryModule,Emailing, AuthService) {
			console.log("Weather Email Controller");
        
        $scope.person= {"name":"",
                        "email":"",
                        "countryid":""};
        $scope.countries = [];
                                
        $scope.contacts = [];
        var state = "add";

        $scope.displayContactModal= function(){
            state = "add";
            $("#contactModal").modal("show");
        };
        
        function loadCountries() {
				$scope.country_loading = true;
				Country.get($scope.userCountry).then(function (res) {
          $scope.countries = res.data;
          $scope.country_loading = false;
				});
			}
        
        $scope.saveContact = function(data) {
            console.log("here");
            console.log(data); 
            if (state === "add"){
            Emailing.add(data).then(function (res) {
                console.log(res);
                if (res.status ===201){
                    swal('Adding Contact', 'Successfully Added Contact', 'success');                    
                    $("#contactModal").modal("hide");                    
                    getContacts();
        
                }
                else{
                    swal('Adding Contact', 'Error Adding Contact', 'error');                    
                }
            });
            }
            else{
                Emailing.update(data).then(function (res) {
                    console.log(res);
                    if (res.status ===200){
                        swal('Updating Contact', 'Successfully Updated Contact', 'success');                    
                        $("#contactModal").modal("hide");                    
                        getContacts();
            
                    }
                    else{
                        swal('Adding Contact', 'Error Adding Contact', 'error');                    
                    }
                });
            }

        };



        function getContacts(){
            Emailing.get().then(function (res) {
                if (res.data) {    
                    $scope.contacts = res.data;
                    console.log($scope.countries);                 
                    $scope.country_loading = false;
                }
            });
        }
     
        $scope.deleteContact = function(data) {
            Emailing.delete(data.id).then(function (res) {
                console.log(res);
                if (res.status ===201){
                    swal('Delete Contact', 'Successfully Deleted Contact', 'success');   
                    getContacts();                 
                    $("#contactModal").modal("hide");                    
                }
                else{
                    swal('Delete Contact', 'Error Deleting Contact', 'error');                    
                }
            });

        };


        $scope.editContact = function(data){
            state = "edit";
            $scope.person = data;
            $scope.person.name = data.contactperson;
            $("#contactModal").modal("show");
        };

        loadCountries(); 
        getContacts();
        loadCountries();                        
        
        
        

        }]);