// angular.module('mfisheries.Weather')
// .service("Emailing", ["$http", function($http){
//     var weatherEmail = {};
//
//     weatherEmail.add = function(data){
//         return $http.post('/api/weather/email', data);
//     };
//
//     weatherEmail.get = function(id){
//         if (id && id !== 0){
//         return $http.get('/api/weather/email'+id);
//         }
//         return $http.get('/api/weather/email');
//
//     };
//     weatherEmail.delete = function(id){
//         return $http.delete('/api/weather/email/'+id);
//     };
//     weatherEmail.update = function(data){
// 		return $http.put('/api/weather/email/' + data.id, data);
// 	};
//     return weatherEmail;
// }]);


class Emailing extends BaseAPIService{
	/**
	 *
	 * @param $http
	 */
	constructor($http){
		super($http, "/api/weather/email");
	}
}

Emailing.$inject = [
	"$http"
];
angular.module('mfisheries.Weather')
		.service("Emailing",Emailing);
