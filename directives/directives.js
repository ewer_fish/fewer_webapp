'use strict';
// directives.js

angular.module('mfisheries.directives',[]).
// https://uncorkedstudios.com/blog/multipartformdata-file-upload-with-angularjs
directive('fileModel', ['$parse', function($parse){
		return {
			restrict : "A",
			link : function(scope, element, attrs){
				var model = $parse(attrs.fileModel),
				modelSetter = model.assign;

				element.bind('change', function(){
					scope.$apply(function(){
						modelSetter(scope, element[0].files[0]);
					});
				});
			}
		};
	}])

.directive('whenScrolled', function() {
    return function(scope, elm, attr) {
        var raw = elm[0];
        
        elm.bind('scroll', function() {
            setTimeout(function(){ 
                if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
                     scope.$apply(attr.whenScrolled);                                        
                }
            }, 1000);
        });
    };
});