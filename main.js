"use strict";
// main.js
angular.module('mfisheries', [
	'mfisheries.controllers',
	'mfisheries.services',
	'mfisheries.factories',
	'mfisheries.directives',
	'mfisheries.filters',
	'mfisheries.User',
	'mfisheries.Country',
	'mfisheries.Occupation',
	'mfisheries.Village',
	'mfisheries.CAP',
	'mfisheries.Config',
	'mfisheries.DMReport',
	'mfisheries.Procedures',
	'mfisheries.Weather',
	'mfisheries.FEWERModule',
	'mfisheries.EmergencyModule',
	'mfisheries.MissingPersons',
	'mfisheries.Alerts',
	'mfisheries.LEK',

	'mfisheries.CGControllers',
	'mfisheries.AdminServices',
	'mfisheries.ImageDiaryController',
	'mfisheries.AlertGroupController',
	'mfisheries.GalleryController',
	'mfisheries.TripHistoryController',
	'mfisheries.MapHelperServices',
	'mfisheries.MapServices',
	'mfisheries.LekServices',
	'mfisheries.AdminLEKController',
	'mfisheries.CoastguardServices',
	
	// Angular Modules
	'bootstrapLightbox',
	'angularValidator',
	'ngRoute',
	'ui.grid',
	'ui.grid.edit',
	'firebase',
	'angulartics',
	'angulartics.google.analytics',
	'ngSanitize',
	'ngAnimate',
	'apMesa',
	'datatables'
])
	.controller("ApplicationController", function ($scope, USER_ROLES, AuthService, Session, $window, LocalStorage) {
		const APP_VERSION = 7;
		$scope.currentUser = LocalStorage.getObject('user');
		$scope.userRoles = USER_ROLES;
		$scope.isAuthorized = AuthService.isAuthorized;
		
		$scope.setCurrentUser = function (user) {
			$scope.currentUser = user;
		};
		
		$scope.redirectHome = function () { };
		
		$scope.logout = function () {
			Session.destroy();
			$scope.setCurrentUser(null);
			//TODO Ensure Firebase session is logged out as well
			if ($window && $window.gapi) {
				var auth2 = $window.gapi.auth2.getAuthInstance();
				auth2.signOut().then(function () {
					console.log('User signed out.');
				});
			}
			// $window.location.reload();
		};
		
		$("#main_menu").show();
		
		//checking the application version (if version is different, clear cache)
		if (!LocalStorage.hasKey("version") || parseInt(LocalStorage.get("version")) !== APP_VERSION) {
			LocalStorage.clearCache();// clear all
			LocalStorage.set("version", APP_VERSION);// set app version
			$scope.logout();
		}
	})
	
	.run(function ($rootScope, AUTH_EVENTS, AuthService, USER_ROLES, $location, LocalStorage) {
		const publicRoutes = [
				'/login',
				'/user/info',
				'/test',
				'/weather/report',
					'/lek',
				// FEWER Capabilities for general user
				'/fewer',
				'/fewer/dmreports',
				'/fewer/emergencycontacts',
				'/fewer/procedures',
				'/fewer/alerts',
				"/fewer/missingpersons"
			],
			
			agentsRoutes = [
				'/all/tracks'
			],
			
			fisherUserRoutes = [
				'/image/diary',
				'/image/gallery',
				'/lek',
				'/user/profile',
				'/trip/history',
				'/weather/report',
				'/user/info/',
				// FEWER Capabilities for Fishers
				'/fewer',
				'/fewer/dmreports',
				'/fewer/emergencycontacts',
				'/fewer/procedures',
				'/fewer/alerts',
				'/fewer/missingpersons'
			],
			
			adminRoutes = [
				'/admin/users',
				'/admin/country',
				'/admin/modules',
				'/admin/occupations',
				'/admin/villages',
				'/admin/config',
				'/admin/weather',
				'/admin/email',
				
				'/admin/lek',
				'/admin/miscellaneous',
				'/admin/emergencycontacts',
				'/admin/fewer',
				'/admin/fewer/dmreports',
				'/admin/fewer/missingpersons',
				'/alert/groups',
				'/user/info',
				'/user/profile',
				'/user/tracks',
				'/weather/report',
				'/fewer',
				'/fewer/dmreports',
				'/testing',
				'/lek',
				'/dashboard'
			];
		
		function routeClean(routes, route) {
			return _.find(routes, noAuthRoute => { // Produces true for user info with modules
				// console.log("Route %s checked against %s produces %s", route, noAuthRoute, route.includes(noAuthRoute));
				return route.includes(noAuthRoute);
			});
		}
		
		// Retrieve the role information from the user type submitted from login process
		function getRole(roles, userRole) {
			for (let r in roles) {
				if (roles.hasOwnProperty(r)) { // If the code matches the role's code return value
					if (roles[r].code === userRole) return roles[r];
				}
			}
		}
		
		function redirectToDefaultURL(roleID) {
			const role = getRole(USER_ROLES, roleID);
			const path = role.default_path;
			$location.path(path);
		}
		
		// Run after each time route changes to check for permissions
		$rootScope.$on('$routeChangeStart', function (event, next, current) {
			const url = $location.url();
			
			if (!AuthService.isAuthenticated()) {    // First Check if User logged In
				if (!routeClean(publicRoutes, url)) {  // Check is the route is not public then redirect to login page
					$location.path('/login');
				}
			} else {                                  // User is logged in
				const roleID = LocalStorage.getObject('user').userRole;
				if (url === '/' || url === '/login') {
					redirectToDefaultURL(roleID);
				}
				else if (!routeClean(agentsRoutes, url) && roleID === 1) {
					redirectToDefaultURL(roleID);
				}
				else if (!routeClean(fisherUserRoutes, url) && roleID === 3) {
					redirectToDefaultURL(roleID);
				}
				else if (!routeClean(adminRoutes, url) && (roleID === 2 || roleID === 4 || roleID === 6)) {
					redirectToDefaultURL(roleID);
				}
			}
			
		});
		
		// Receiver for broadcast that will be triggered when successfully logged in
		$rootScope.$on('auth-login-success', function (event, userType) {
			// The page is being viewed as an embedded webview
			if (window.mFisheriesAndroid) {
				console.log("mFisheries Android Detected");
				window.mFisheriesAndroid.completedRegistration();
				return;
			}
			// Viewed on a web browser
			const role = getRole(USER_ROLES, userType.userRole);
			// Control Redirection when logged in
			const path = role.default_path;
			// console.log("Authentication was successful for: %s and directed to %s", JSON.stringify(userType), path);
			$location.path(path);
		});
		// Receiver for broadcast that will be triggered when a new Google user logs in
		$rootScope.$on('auth-new-googleuser', function (event, modules) {
			$location.path('/user/info/' + modules.link);
		});
		
	})
	// Configuration for enabling debugging information for development
	.config(['$compileProvider', function ($compileProvider) { // Based on https://docs.angularjs.org/guide/production
		$compileProvider.debugInfoEnabled(true);
	}])
	// Set the API needed for communication with the web services
	.config(["$httpProvider", function ($httpProvider) {
		$httpProvider.interceptors.push(function ($q) {
			return {
				"request": function (config) {
					config.headers.apikey = "DUaeaX9LORnFPvewmjWVnAv7i9gT9FRX";
					return config;
				},
				"response": function (response) {
					return response;
				}
			};
		});
	}])
	//Configuration for Lightbox Provider used in maps
	.config(function (LightboxProvider) {
		LightboxProvider.templateUrl = 'static/partials/lightbox.pt';// set a custom template
	});
